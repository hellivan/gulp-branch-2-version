'use strict';

var through = require('through2');
var pluginError = require('plugin-error');
var gutil = require('gulp-util');
var gitRev = require('git-rev');
var _ = require('lodash');

var MODULE_NAME = 'gulp-branch-2-version';


// Preserve new line at the end of a file
// copied from gulp-bump
function possibleNewline(json) {
	var lastChar = (json.slice(-1) === '\n') ? '\n' : '';
	return lastChar;
}


// Figured out which "space" params to be used for JSON.stringfiy.
// copied from gulp-bump
function space(json) {
	var match = json.match(/^(?:(\t+)|( +))"/m);
	return match ? (match[1] ? '\t' : match[2].length) : '';
}


// leaned on gulp-bump
module.exports = function (opts){

	var templateOpts = {
		append: true,
		force: false,
	}

	opts = _.merge(templateOpts, opts);
	
    return through.obj(function(file, enc, cb){
		if (file.isNull()) {
			return cb(null, file);
		}

		if (file.isStream()) {
			return cb(new pluginError(MODULE_NAME, 'Streaming not supported!'));
		}

		var content = String(file.contents);
		var json;

		try {
			json = JSON.parse(content);
		} catch (e) {
			return cb(new pluginError(MODULE_NAME, 'Problem parsing JSON file', {
				fileName: file.path,
				showStack: true
			}));
		}

		if(!json.version){
			return cb(new pluginError(MODULE_NAME, 'No version-field detected!'));
		}
		
		return gitRev.branch(function (branch) {

			if(opts.force || (opts.append && branch !== 'master')){
				gutil.log('Appending branch', branch, ' to version ', json.version);
				json.version = json.version + "-" + branch;
				file.contents = new Buffer(
					JSON.stringify(json, null, opts.indent || space(content))
						+ possibleNewline(content)
				);
			}
			
			return cb(null, file);
		})
    });
}
